#improved version of acp_times.py using table driven

import math
import arrow

speed_open = [34,32,30,28,26]
speed_close = [15,15,15,11.428,13.333]
distancia = [0,200,400,600,1000,1300]

def tiempo(acumulado):#separate a float number to a hours and minutes format.
    resultados =[]
    minutos, horas = math.modf(acumulado)
    resultados.append(round(horas))
    resultados.append(round(60*minutos))
    return resultados #[Horas, minutos]

def calculo(distance, speed):#it gets the distance and the speed and returns a float that indicate the time in hours of how much it takes to bike that distance
    resultados = []
    minutos, horas = math.modf(distance/float(speed))#separate the integer part from the decimal part.
    resultados.append(round(horas))
    resultados.append(round(60*minutos))
    return resultados #[Horas, minutos]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    temp = control_dist_km
    acumulado = 0.0
    start = arrow.get(brevet_start_time)
    start = start.replace(tzinfo='US/Pacific')#correct the time.
    hours = 0
    minutes = 0
    error = arrow.get("2001-01-01 00:00")
    error = error.replace(tzinfo='US/Pacific')#correct the time.


    if control_dist_km <= brevet_dist_km: #control can be up to 10% off the brevet distance. for instance, the total distance is 610km , the last control could be at 602 km.
        for i in range(0,len(distancia)-1):
            if control_dist_km > distancia[i+1]:
                acumulado+=(distancia[i+1]-distancia[i])/float(speed_open[i])
                temp = temp - (distancia[i+1]-distancia[i]);
            else:
                acumulado+=temp/float(speed_open[i])#the last part and then breaks
                break

        hour, minute = tiempo(acumulado)
        start = start.shift(hours = hour, minutes = minute)
    else:
        return error.isoformat()

    return start.isoformat()

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    temp = control_dist_km
    acumulado = 0.0
    start = arrow.get(brevet_start_time)
    start = start.replace(tzinfo='US/Pacific')#correct the time.
    hour = 0
    minute = 0
    error = arrow.get("2001-01-01 00:00")#incase of error, it returns january first 2001 at 00:00
    error = error.replace(tzinfo='US/Pacific')#correct the time.

    if control_dist_km <= brevet_dist_km: #control can be up to 10% off the brevet distance. for instance, the total distance is 610km , the last control could be at 602 km.
        if control_dist_km <= 60:
            acumulado = control_dist_km/float(20)
            hour, minute = tiempo(acumulado)
            hour+=1
        else:
            for i in range(0,len(distancia)-1):
                if control_dist_km > distancia[i+1]:
                    acumulado+=(distancia[i+1]-distancia[i])/float(speed_close[i])
                    temp = temp - (distancia[i+1]-distancia[i]);
                else:
                    acumulado+=temp/float(speed_close[i])
                    break
            hour, minute = tiempo(acumulado)

        start = start.shift(hours = hour, minutes = minute)
    else:
        return error.isoformat()

    return start.isoformat()
