import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import flask
import arrow
import acp_times
import config
import logging

app = Flask(__name__)
#mongodb
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
#configuration
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY#from credentials

contador = 0#too keep track which query is being look up
db.tododb.delete_many({})#delete all info from the previos request


#main
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route('/vacio')#in case submit has no data filled
def vacio():
    return render_template('vacio.html')

#
@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/_calc_times")#from project 4(with an acp_times.py improved)
def _calc_times():
    km = request.args.get('km', 999, type=float)#This block gets variables from the script in html
    dist_km = request.args.get('brevet_dist_km', 999, type=float)
    hour = request.args.get('begin_time')
    date = request.args.get('begin_date')
    tiempo_completo = "{} {}".format(date, hour)

    open_time = acp_times.open_time(km, dist_km, tiempo_completo)#send the parameter to the calculator.
    close_time = acp_times.close_time(km, dist_km, tiempo_completo)
    if km <= dist_km:
        notes = "Correct"
    else:
        notes = "Incorrect"

    result = {"open": open_time, "close": close_time, "notes": notes}#to js.

    return flask.jsonify(result=result)

################### LOGIC FOR THE DB ########################

###for displaying
@app.route('/display', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    if len(items) == 0:#
        return render_template('vacio.html')#if no items, show vacio
    else:
        return render_template('todo.html', items=items)

###submit###
@app.route('/submit', methods=['POST'])
def new():
    #values gather from the form
    open = request.form.getlist("open")
    close = request.form.getlist("close")
    km = request.form.getlist("km")
    millas = request.form.getlist("miles")
    notes = request.form.getlist("notes")

    lenght = 0#loop to check how many elements have the list
    for i in open:
        if i is not "":
            lenght+=1

    if lenght == 0:
        return redirect(url_for('vacio'))

    global contador
    # temporal = contador
    i = -1
    while i < lenght:#we could have parse the info directly request form but for some reason, during the first query, it wont show the very first data submitted, so we have to put the info first in a list and then passed to json
        if i is not -1:
            item_doc = {
                'contador':contador,# to know which counter is
                'miles': millas[i],
                'km':km[i],
                'open': open[i],
                'close': close[i],
                'notes': notes[i]
            }
        else:
            separador = "Query: " + str(contador)
            item_doc = {
                'contador':"",# to know which counter is
                'miles': "",
                'km': "",
                'open': separador,
                'close': "",
                'notes': ""
            }


        db.tododb.insert_one(item_doc)#insert item to the db
        i+=1

    contador+=1
    return redirect(url_for('index'))

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
