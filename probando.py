import math
import arrow

speed_open = [34,32,30,28,26]
speed_close = [15,15,15,11.428,13.333]
distancia = [0,200,400,600,1000,1300]
# invertido = [1300,1000,600,400,200,0]
def tiempo(acumulado):
    resultados =[]
    #convierte de fraccion a hora luego de haber acumulado.
    minutos, horas = math.modf(acumulado)
    resultados.append(round(horas))
    resultados.append(round(60*minutos))
    return resultados #[Horas, minutos]

def calculo(distance, speed):
    resultados = []
    minutos, horas = math.modf(distance/float(speed))#separate the integer part from the decimal part.
    resultados.append(round(horas))
    resultados.append(round(60*minutos))
    return resultados #[Horas, minutos]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    temp = control_dist_km
    acumulado = 0.0
    brevet_start_time = 
    start = arrow.get(brevet_start_time)
    start = start.replace(tzinfo='US/Pacific')#correct the time.
    hours = 0
    minutes = 0

    if control_dist_km <= brevet_dist_km*1.1: #control can be up to 10% off the brevet distance. for instance, the total distance is 610km , the last control could be at 602 km.
        for i in range(0,len(distancia)-1):
            if control_dist_km > distancia[i+1]:
                # print("Mayor que {}".format(distancia[i+1]))
                acumulado+=(distancia[i+1]-distancia[i])/float(speed_open[i])
                # print("acumulado {}".format(acumulado))
                temp = temp - (distancia[i+1]-distancia[i]);
                # print(temp)
            else:
                acumulado+=temp/float(speed_open[i])
                # print("Final {}".format(acumulado))
                break

        hour, minute = tiempo(acumulado)
        start = start.shift(hours = hour, minutes = minute)
        print("{}:{}".format(hour, minute))
    else:
        start = arrow.now()

    return start.isoformat()

open_time(890, 1000, 0)
