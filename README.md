Brevet calculator for races with Ajax and monboDB to storage data.

Student: Antonio Jesus Silva Paucar Class: 322 Spring 2020

The server will contain the logic for calculating the times when a control post would open and close in a bike race. In order to start the server, we execute the command "docker-compose up --build". This will install the requirements needed to run the program and set up the docker enviroment. We then can check the website looking at "localhost:5000".

I included a file where it explains how I handled the tests cases.


test case number one.

If we don't enter any data into the form and press submit, the website will show us a page where it indicates us that no data have been entered,

test case number two:

If we enter a distance for a brevet that it is outside the boundaries of the race **//(I have set up that a brevet can be outbounds for 10% of the total distance of the race.) no longer!**, then an message in open and close time will be show indicating that date "january first, 2001 at 00:00". It also will show a signal of incorrect in the "notes"

test case number three:
If we add some entries and press submit, it will clear up the page and send the data to the webserver. If at some point we press "display", it will show us the history of all the queries that we have done during our session.

**update: I decided to allow brevets controls only inside the outbounds of the total race, since it is possible that a race has a 600km lenght and it ends at 609 but brevets should remain inside the 600km.**
